﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Snake
{
    class JocSnake
    {
        const int WIDTH = 5;
        const int HEIGTH = 5;

        Point cap;
        DireccioSnake direccio;

        public JocSnake()
        {
            cap = new Point(0, 0);
            direccio = DireccioSnake.Sud;
        }

        public DireccioSnake Direccio { get => direccio; set => direccio = value; }
        public Point Cap { get => cap; }

        public void Moure()
        {
            if (direccio == DireccioSnake.Nord)
            {
                cap.Y--;
                if (cap.Y < 0)
                    cap.Y = HEIGTH - 1; 
            }
            else if(direccio == DireccioSnake.Sud)
            {
                cap.X++;
                if (cap.X == WIDTH)
                    cap.X = 0;
                
            }
            else if(direccio == DireccioSnake.Est)
            {
                cap.Y++;
                if (cap.Y == HEIGTH)
                    cap.Y = 0;
            }
            else
            {
                cap.X--;
                if (cap.X == 0)
                    cap.X = WIDTH - 1;
            }
        }

    }

    public enum DireccioSnake
    {
        Nord,
        Sud,
        Est,
        Oest
    }
}

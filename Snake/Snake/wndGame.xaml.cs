﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Snake
{
    /// <summary>
    /// Lógica de interacción para wndGame.xaml
    /// </summary>
    public partial class wndGame : Window
    {
        JocSnake joc;
        DispatcherTimer timer;
        SolidColorBrush pinzellSnake = new SolidColorBrush(Colors.Green);
        const int TAMANY_SNAKE = 100;

        public wndGame()
        {
            InitializeComponent();

            joc = new JocSnake();
            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
            timer.Interval = TimeSpan.FromSeconds(1);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //Pintasion
            canvas.Children.Clear();

            Ellipse elSnake = new Ellipse()
            {
                Fill = pinzellSnake,
                Width = TAMANY_SNAKE,
                Height = TAMANY_SNAKE
            };

            Canvas.SetLeft(elSnake, joc.Cap.Y*100);
            Canvas.SetTop(elSnake, joc.Cap.X*100);

            canvas.Children.Add(elSnake);
            joc.Moure();
        }

        private void btnInici_Click(object sender, RoutedEventArgs e)
        {
            timer.Start();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Snake
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int TAMANY_SNAKE = 50;
        SolidColorBrush pinzellSnake = new SolidColorBrush(Colors.Green);
        SolidColorBrush pinzellPoma = new SolidColorBrush(Colors.Red);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnInici_Click(object sender, RoutedEventArgs e)
        {
            //Random r = new Random();
            //Ellipse elSnake = new Ellipse()
            //{
            //    Fill = pinzellSnake,
            //    Width = TAMANY_SNAKE,
            //    Height = TAMANY_SNAKE
            //};

            //Canvas.SetLeft(elSnake, r.Next(0,300));
            //Canvas.SetTop(elSnake, r.Next(0, 300));

            //canvas.Children.Add(elSnake);

            //Ellipse elPoma = new Ellipse()
            //{
            //    Fill = pinzellPoma,
            //    Width = TAMANY_SNAKE,
            //    Height = TAMANY_SNAKE
            //};

            //Canvas.SetLeft(elPoma, r.Next(0, 300));
            //Canvas.SetTop(elPoma, r.Next(0, 300));

            //canvas.Children.Add(elPoma);
            Window joc = new wndGame();
            joc.ShowDialog();
            this.Close();
        }
    }
}
